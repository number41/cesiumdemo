/// <reference path="../node_modules/immutable/dist/immutable.d.ts" />
/// <reference path="main/ambient/cesium/cesium.d.ts" />
/// <reference path="main/ambient/jquery/jquery.d.ts" />
/// <reference path="main/ambient/lodash/lodash.d.ts" />
/// <reference path="main/ambient/react-redux/react-redux.d.ts" />
/// <reference path="main/ambient/react/react-dom.d.ts" />
/// <reference path="main/ambient/react/react.d.ts" />
/// <reference path="main/ambient/redux-actions/redux-actions.d.ts" />
/// <reference path="main/ambient/redux/redux.d.ts" />

interface AppConfigs {
    geoserver_url: string;
}

interface Window {
    GLOBAL_CONFIGS?: AppConfigs;
}