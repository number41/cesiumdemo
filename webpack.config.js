var failPlugin = require('webpack-fail-plugin');

var configs = {
    entry: {
        app: "./app/main.tsx"
    },
    output: {
        path:   __dirname + "/public",
        filename:   "bundle.[name].js"
    },
    devtool: 'source-map',
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "ts-loader" }
        ],
        noParse: []
    },
    resolve: {
        alias: {},
        extensions: ['', '.ts', '.tsx', '.js', '.jsx']
    },
    plugins: [failPlugin]
}

module.exports = configs;
