/// <reference path="../../typings/main.d.ts" />

import * as React from 'react';
import { CesiumContainer } from '../components/CesiumContainer';
import { LayersView } from '../components/LayersView';

/** State for RootComponent */
interface RootState {
    /** The primary Cesium view */
    widget: Cesium.Viewer;
}

/** Root component for the entire application. */
export default class RootComponent extends React.Component<{}, RootState> {
   constructor() {
       super();
       this.state = {
           widget: null
       };
   }
   
   private _onPublishWidget(widget: Cesium.Viewer) {
       this.setState({
           widget: widget
       });
   }
   
   /**
    * Toggles layer between being shown and being hidden
    * @param index Index of layer to toggle
    */
   private _onToggleLayer(index: number): void {
       let layer = this.state.widget.imageryLayers.get(index);
       layer.show = !layer.show;
       this.setState(this.state);
   }
   
   render() {
       let layers = this.state.widget ? this.state.widget.imageryLayers : null;
       return (
           <div className='cesiumContainer'>
                <CesiumContainer
                    publishWidget={this._onPublishWidget.bind(this)}/>
                <LayersView layers={layers}
                            toggleLayer={this._onToggleLayer.bind(this)} />
           </div>
       );
   }
};
