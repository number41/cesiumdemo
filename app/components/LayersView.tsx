/// <reference path="../../typings/main.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { NamedImageryLayer } from '../models/interfaces';
import { List } from 'immutable';

interface LVProps {
    layers: Cesium.ImageryLayerCollection;
    toggleLayer: (index: number) => void;
}

export const LayersView = (props: LVProps) => {
    const { layers, toggleLayer } = props;
    let data: NamedImageryLayer[] = [];
    const length = layers ? layers.length : 0;
    for (let i = 0; i < length; ++i) {
        data.push(layers.get(i));
    }
    
    return (
        <div className='layers-view '>
            <table className="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Enabled</th><th>Name</th><th>Alpha</th>
                    </tr>
                </thead>
                <tbody>
                    {data.reverse().map((layer, index) => 
                        <tr key={index}>
                          <td><input 
                                type='checkbox'
                                onChange={() => toggleLayer(length - 1 - index) } 
                                checked={layer.show} /></td>
                          <td>{layer.name}</td>
                          <td>{layer.alpha}</td>
                        </tr>)}
                </tbody>
            </table>
        </div>
    ); 
}
