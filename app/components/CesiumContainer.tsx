/// <reference path="../../typings/main.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { NamedImageryLayer } from '../models/interfaces';

/**
 * Factory function for creating a NamedImageryLayer
 * @param name Name of the layer
 * @param provider ImageryProvider for the layer
 * @param alpha Initial alpha of the layer
 * @returns new NamedImageryLayer
 */
function createLayer(name: string, provider: Cesium.ImageryProvider, alpha = 1.0) {
    let layer: NamedImageryLayer = new Cesium.ImageryLayer(provider);
    layer.alpha = alpha;
    layer.name = name;
    return layer;
}

/* The initial layers to add once the widget has been created */
const initialLayers = [
    createLayer("Stamen", Cesium.createOpenStreetMapImageryProvider({
        url: '//stamen-tiles.a.ssl.fastly.net/watercolor',
        fileExtension: 'jpg',
        credit: 'Map tiles by Stamen Design, under CC BY 3.0 Data by OpenStreetMap, under CC BY SA.'
    })),
    createLayer("Grid", new Cesium.GridImageryProvider(), 0.5),
    createLayer("US Weather Radar", new Cesium.WebMapServiceImageryProvider({
        url: '//mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0r.cgi?',
        layers: 'nexrad-n0r',
        credit: 'Radar data courtesy of Iowa Environmental Mesonet',
        parameters: {
            transparent: 'true',
            format: 'image/png'
        }
    }), 0.5)
];

/**
 * Props interface for CesiumContainer 
 */
export interface CCProps {
    /** Callback for broadcasting the Cesium viewer out of this componentDidMount
     *  @param widgit Newly created Cesium.Viewer object
     */
    publishWidget: (widgit: Cesium.Viewer) => void;
} 


/** Creates, mounts, and publishes the Cesium.Viewer object */
export class CesiumContainer extends React.Component<CCProps, {}> {

    private widget: Cesium.Viewer;
    private node: Element;

    constructor() {
        super();
    }

    render() {
        // Return an empty div. The widgit gets hooked on after
        return <div className='cesiumContainer'/>;
    }

    componentDidMount() {
        this.node = ReactDOM.findDOMNode(this);
        this.widget = new Cesium.Viewer(this.node, {
            baseLayerPicker: false
        });
        
        const layers = this.widget.imageryLayers;
        layers.removeAll(true);
        initialLayers.forEach((layer) => layers.add(layer));
        
        this.props.publishWidget(this.widget);
    } 

    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.node);
        // Any Cesium calls for cleanup?
    }
    
    shouldComponentUpdate() {
        return false;
    }
}
