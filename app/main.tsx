/// <reference path="../typings/main.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom'
import RootComponent from './containers/Root';

console.log(`Global URL: ${window.GLOBAL_CONFIGS.geoserver_url}`);

/** Primary entry point into the application */
ReactDOM.render(
     <RootComponent />
    ,
    document.getElementById('mount')
);
