/// <reference path="../../typings/main.d.ts" />

/** We'll be slapping on a name directly to all ImageryLayers. Woo! */
export type NamedImageryLayer = Cesium.ImageryLayer & {name?: string};


