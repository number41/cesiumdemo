SOURCES := $(shell find app/ -type f -name '*.ts' -o -name '*.tsx')

.PHONY: init
init:
	yarn install

public:
	mkdir -p public/

public/Cesium: public node_modules/cesium/Build/Cesium
	mkdir -p public/Cesium
	cp -r node_modules/cesium/Build/Cesium/* public/Cesium

public/bundle.app.js: public ${SOURCES}
	webpack --config webpack.config.js
    
.PHONY: build
build: public/Cesium public/bundle.app.js
    
.PHONY: clean
clean:
	rm -rf public/*
    
.PHONY: docs
docs:
	./node_modules/.bin/typedoc --mode file --target ES5 --module commonjs --out dist/doc app/
